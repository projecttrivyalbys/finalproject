#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <WinSock2.h>
#include <Windows.h>
#include "User.h"
#include "Helper.h"
using namespace std;
class User;
class Room
{
private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionsNo;
	string _name;
	int _id;
public:
	Room(int id, User* admin, string name, int maxUsers, int questionsNo, int questionTime);
	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuestionNo();
	int getQuestionTime();
	int getId();
	string getName();
	int getMaxUsers();
private:
	string getUsersAsString(vector<User*> usersList, User* excludeUser);
	void sendMessage(string message);
	void sendMessage(string message, User* excludeUser);


};