#include "Game.h"

int Game::_id = 0; //static var 
Game::Game(const vector<User*> &_players, int _qustions_no, DataBase& _dbb) : _db(_dbb)
{
	_id++;//count the number of users
	if (_db.insertNewGame() == -1)
		throw exception();
	else
	{
		_questions = _db.initQuestions(_qustions_no);//reset randmly the Q
		this->_players = _players;
		for each (User* u in _players)
		{
			_results.insert(pair<string, int>(u->getUsername(), 0));// reset the results
			u->setGame(this);//set the game to each user in the room
		}
	}
	this->_qustions_no = _qustions_no;
}
Game::~Game()
{
	for each (Question* q in _questions)
		delete q;
	delete &_players;
	delete &_questions;
}
void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}
void Game::handleFinishGame()//building finish game massege
{
	string s = "121";
	s += Helper::getPaddedNumber(_players.size(), 1);
	_db.updateGameStatus(_id);
	for each (User* u in _players)
	{
		s += Helper::getPaddedNumber(u->getUsername().size(), 2);//username size
		s += u->getUsername();//username
		s += Helper::getPaddedNumber(_results[u->getUsername()], 2);//results
	}
	for each (User* u in _players)//sending the massege
	{
		try{
			u->send(s);
		}
		catch (...)
		{
		}
		u->setGame(nullptr);
	}
}
bool Game::handleNextTurn()
{
	if (_players.empty())//if there are players in the game
	{
		handleFinishGame();
		return false;
	}
	else
	{
		if (currentTurnAnswers == _players.size())//check if all the players answered
		{
			if ((_currQuestionIndex+1) == _qustions_no)//check if this is the last Q
			{
				handleFinishGame();
				return false;
			}
			else 
			{
				_currQuestionIndex++;
				sendQuestionToAllUsers();
				return true;
			}
		}
	}
}
bool Game::handleAnswersFromUser(User* user, int _answer_no, int time)
{
	bool isCorrect = false;
	string ans;
	currentTurnAnswers++;
	string username = user->getUsername();
	int id = _questions[_currQuestionIndex]->getId();

	if (_questions[_currQuestionIndex]->getCorrectAnswerIndex() == _answer_no - 1)// if he answerd correctly
	{
		_results.at(user->getUsername())++;
		isCorrect = true;
	}
	if (_answer_no == 5)//if the player didnt answerd in time
		ans = "";
	else
		ans = _questions[_currQuestionIndex]->getAnswers()[_answer_no - 1];

	
    _db.addAnswerToPlayer(getId(), username, id, ans, isCorrect, time);//update the player result and time...
	if (isCorrect)
		user->send("1201");
	else
		user->send("1200");
	return handleNextTurn();
}
bool Game::leaveGame(User* user)
{
	auto it = find(_players.begin(), _players.end(), user);//serch the user and erase him from the list
	if (it != _players.end())
	{
		_players.erase(it);
		handleNextTurn();
		return true;
	}
	return false;
}
int Game::getId()
{
	return _id;
}

void Game::sendQuestionToAllUsers()
{
	string s = "118";
	s += Helper::getPaddedNumber(_questions[_currQuestionIndex]->getQuestion().size(), 3);//Q size
	s += _questions[_currQuestionIndex]->getQuestion();//the Q
	for (int i = 0; i < 4; i++)//handle the answers
	{
		string help = _questions[_currQuestionIndex]->getAnswers()[i];
		s += Helper::getPaddedNumber(help.size(), 3);
		s += help;
	}
	currentTurnAnswers = 0;
	for each (User* u in _players)//send it to players
	{
		u->send(s);
	}
}