#include "Room.h"

Room::Room(int id, User* admin, string name, int maxUsers, int questionsNo, int questionTime)
{
	_id = id;
	_admin = admin;
	_name = name;
	_maxUsers = maxUsers;
	_questionsNo = questionsNo;
	_questionTime = questionTime;
	_users.push_back(admin);
}

int Room::getMaxUsers()
{
	return _maxUsers;
}
bool Room::joinRoom(User* user)
{
	if (_users.size() >= _maxUsers)
	{
		user->send("1101"); //room is full
		return false;
	}
	else
	{
		_users.push_back(user);// add the user to the list of users in room
		user->send("1100"+ Helper::getPaddedNumber(getQuestionNo(),2)+Helper::getPaddedNumber(getQuestionTime(),2));
		sendMessage(getUsersListMessage());
		return true;
	}
}

void Room::leaveRoom(User* user)
{
	if (find(_users.begin(), _users.end(), user) != _users.end())//search the user
	{
		_users.erase(std::remove(_users.begin(), _users.end(), user), _users.end());//erase him
		user->send("1120");
		sendMessage(getUsersListMessage(), user);
	}
}

int Room::closeRoom(User* user)
{
	if (user == _admin)//check if the user is the admin
	{
		for each (User* u in _users)//remove all the users
		{
			u->send("116");
			u->clearRoom();
		}
		return _id;
	}
	else
	{
		return -1;
	}
}

vector<User*> Room::getUsers()
{
	return _users;
}

string Room::getUsersListMessage()
{
	string listUsers = "108";
	if (_users.empty())
	{ 
		return "1080";
	}
	else
	{
		listUsers+= Helper::getPaddedNumber(_users.size(), 1);//users size
		for each (User* u in _users)
		{
			listUsers += Helper::getPaddedNumber(u->getUsername().length(), 2);//username length
			listUsers += u->getUsername();//username
		}
		return listUsers;
	}
}

int Room::getQuestionNo()
{
	return _questionsNo;
}

int Room::getQuestionTime()
{
	return _questionTime;
}

int Room::getId()
{
	return _id;
}

string Room::getName()
{
	return _name;
}

string Room::getUsersAsString(vector<User*> usersList, User* excludeUser)
{
	string str;
	for each (User* user in usersList)//pass on all users
	{
		if (user != excludeUser)//check if the user is excluded
			str += user->getUsername() + " ";//add to string
	}
	return str;
}

void Room::sendMessage(string message)
{
	sendMessage(message, nullptr);
}

void Room::sendMessage(string message, User* excludeUser)//send to all users accept the excludeUser
{
	try
	{
		for each (User* user in _users)
		{
			if (user != excludeUser)
				user->send(message);
		}
	}
	catch (exception e)
	{
		cout << e.what();
	}
}