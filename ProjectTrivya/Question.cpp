#include "Question.h"

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	_id = id;
	_question = question;
	//Randomly put answers. Random the index of correct answer and then put on the other indexex the wrong ans.
	int output = 0 + (rand() % (int)(3 - 0 + 1));
	_correctAnswerIndex = output;
	_Answers[output] = correctAnswer;
	if (output == 0)
	{
		_Answers[1] = answer2;
		_Answers[2] = answer3;
		_Answers[3] = answer4;
	}
	else if (output == 1)
	{
		_Answers[0] = answer2;
		_Answers[2] = answer3;
		_Answers[3] = answer4;
	}
	else if (output == 2)
	{
		_Answers[1] = answer2;
		_Answers[0] = answer3;
		_Answers[3] = answer4;
	}
	else if (output==3)
	{
		_Answers[1] = answer2;
		_Answers[2] = answer3;
		_Answers[0] = answer4;
	}
}

string* Question::getAnswers()
{
	return _Answers;
}

int Question::getCorrectAnswerIndex()
{
	return _correctAnswerIndex;
}

int Question::getId()
{
	return _id;
}

string Question::getQuestion()
{
	return _question;
}