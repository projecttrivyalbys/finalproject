#pragma once

#include <vector>
#include <string>
#include <WinSock2.h>
#include <Windows.h>
#include "Room.h"
#include "Game.h"
#include "DataBase.h"

using namespace std;
class Room;
class Game;

class User
{
private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
public:
	User(string username, SOCKET sock);
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void send(string message);
	void setGame(Game* gm);
	void clearGame();
	void createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime);
	bool joinRoom(Room* newRoom);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
	void clearRoom();
};