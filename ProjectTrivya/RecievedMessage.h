#pragma once

#include <string>
#include <vector>
#include <WinSock2.h>
#include <Windows.h>
#include "User.h"

using namespace std;

class RecievedMessage
{
public:
	RecievedMessage(SOCKET sock, int messageCode);

	RecievedMessage(SOCKET sock, int messageCode, vector<string> values);

	SOCKET getSock();
	int getMessageCode();
	User* getUser();
	void setUser(User* user);
	vector<string>& getValues();

private:
	SOCKET _sock;
	int _messageCode;
	vector<string> _values;
	User* _user;
};

