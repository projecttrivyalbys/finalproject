#include "User.h"
#include "Helper.h"

User::User(string username, SOCKET sock)
{
	_username = username;
	_sock = sock;
	_currGame = nullptr;
	_currRoom = nullptr;
}

void User::send(string message)
{
	Helper::sendData(_sock, message);
}

void User::setGame(Game* gm)
{
	_currGame = gm;
	_currRoom = nullptr;
}

void User::clearGame()
{
	_currGame = nullptr;
}

void User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (_currRoom != nullptr)// chek if there is a room
	{
		send("1141");
	}
	else
	{
		_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);// create a ne room
		send("1140");
	}
}

string User::getUsername()
{
	return _username;
}
SOCKET User::getSocket()
{
	return _sock;
}
Room* User::getRoom()
{
	return _currRoom;
}
Game* User::getGame()
{
	return _currGame;
}

bool User::joinRoom(Room* newRoom)
{
	if (_currRoom != nullptr)// chek if there is a room
	{
		return false;
	}
	else
	{
		_currRoom = newRoom;
		return _currRoom->joinRoom(this);// join to room
	}
}

void User::leaveRoom()
{
	if (_currRoom != nullptr)// chek if there is a room
	{
		_currRoom->leaveRoom(this);//leave room
		_currRoom = nullptr;
	}
}

int User::closeRoom()
{
	if (_currRoom == nullptr)// chek if there is a room
	{
		return -1;
	}
	else
	{
		if (_currRoom->closeRoom(this) != -1)// close room
		{
			delete _currRoom;//delete room
		}
		return 0;
	}
}

bool User::leaveGame()
{
	if (_currGame != nullptr)// chek if there is a room
	{
		_currGame->leaveGame(this);//leave game
		_currGame = nullptr;
		return true;
	}
	return false;
}

void User::clearRoom()
{
	_currRoom = nullptr;
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               