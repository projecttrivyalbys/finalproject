#pragma once

#include <vector>
#include <string>
#include <WinSock2.h>
#include <Windows.h>

using namespace std;

class Question
{
private:
	int _correctAnswerIndex;
	int _id;
	string _question;
	string _Answers[4];
public:
	Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4);

	int getCorrectAnswerIndex();
	int getId();
	string getQuestion();
	string* getAnswers();
};