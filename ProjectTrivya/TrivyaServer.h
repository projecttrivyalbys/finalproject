#pragma once

#include "WSAInitializer.h"

#include <thread>
#include <deque>
#include <queue>
#include <map>
#include <mutex>
#include <condition_variable>



#include "User.h"
#include "Helper.h"
#include "Validator.h"
#include "RecievedMessage.h"
#include "Protocol.h"
// Q: why do we need this class ?
// A: this is the main class which holds all the resources,
// accept new clients and handle them.
class TrivyaServer
{
private:
	std::map<SOCKET, User*> _connectedUsers;
	std::map<int, Room*> _roomsList;
	queue<RecievedMessage*> _queRcvMessages;
	static int _roomIdSequence;
	SOCKET _socket;
	DataBase _db;
	std::queue<RecievedMessage*> _messageHandler;
	std::mutex _mtxRecievedMessages;
	std::condition_variable _msgQueueCondition;
	std::condition_variable _edited;
public:
	TrivyaServer();
	~TrivyaServer();

	void server();
	void bindAndListen();
	void accept();
	void clientHandler(SOCKET sc);
	void safeDeleteUser(RecievedMessage* rm);

	User* handleSignin(RecievedMessage* rm);
	bool handleSignup(RecievedMessage* rm);
	void handleSignout(RecievedMessage* rm);

	void handleLeaveGame(RecievedMessage* rm);
	void handleStartGame(RecievedMessage* rm);
	void handlePlayerAnswer(RecievedMessage* rm);

	bool handleCreateRoom(RecievedMessage* rm);
	bool handleCloseRoom(RecievedMessage* rm);
	bool handleJoinRoom(RecievedMessage* rm);
	bool handleLeaveRoom(RecievedMessage* rm);
	void handleGetUsersInRoom(RecievedMessage* rm);
	void handleGetRooms(RecievedMessage* rm);

	void handleGetBestScores(RecievedMessage* rm);
	void handleGetPersonalStatus(RecievedMessage* rm);

	void handleRecievedMessages();
	void addRecievedMessage(RecievedMessage* rm);
	RecievedMessage* buildRecieveMessage(SOCKET sc, int num);

	User* getUserByName(string name);
	User* getUserBySocket(SOCKET sc);
	Room* getRoomById(int id);
};