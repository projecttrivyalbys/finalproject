#pragma once

#include<iostream>
#include <string>
#include <vector>
#include <map>
#include "Question.h"
#include "User.h"
#include "DataBase.h"
using namespace std;
class User;
class Game
{
private:
	static int _id;
	vector<Question*> _questions;
	vector<User*> _players;
	int _qustions_no;
	int _currQuestionIndex;
	DataBase& _db;
	map<string, int>  _results;
	int currentTurnAnswers;

public:
	Game(const vector<User*> &_players, int _qustions_no, DataBase& _db);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswersFromUser(User* user, int _answer_no, int time);
	bool leaveGame(User* user);
	int getId();
	void sendQuestionToAllUsers();
};