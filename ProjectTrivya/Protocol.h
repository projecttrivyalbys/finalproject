#pragma once

/*
CS -> Client to Server
SC -> Server to Client
*/


#define CS_SIGNIN 200
#define CS_SIGNOUT 201
#define SC_SIGNIN 102
#define CS_SIGNUP 203
#define SC_SIGNUP 104
#define CS_ROOMLIST 205
#define SC_ROOMLIST 106
#define CS_ROOMUSERLIST 207
#define SC_ROOMUSERLIST 108
#define CS_JOINROOM 209
#define SC_JOINROOM 210
#define CS_LEAVEROOM 211
#define SC_LEAVEROOM 112
#define CS_MAKEROOM 213
#define SC_MAKEROOM 114
#define CS_CLOSEROOM 215
#define SC_CLOSEROOM 116
#define CS_STARTGAME 217
#define SC_STARTGAME 118
#define CS_SENDANSWER 219
#define SC_SENDANSWER 120
#define SC_ENDGAME 121
#define CS_LEAVEGAME 222
#define CS_BESTSCORES 223
#define SC_BESTSCORES 124
#define CS_PERSONALSTATUS 225
#define SC_PERSONALSTATUS 126
#define CS_EXIT 299