#include "TrivyaServer.h"
static const unsigned short PORT = 8820;
static const unsigned int IFACE = 0;

int TrivyaServer::_roomIdSequence = 0;

TrivyaServer::TrivyaServer()
{//c'tor
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}
TrivyaServer::~TrivyaServer()
{//delete rooms and users and close the socket
	for each (pair<int, Room*> rp in _roomsList)
	{
		delete rp.second;
	}
	for each (pair<SOCKET, User*> su in _connectedUsers)
	{
		delete su.second;
	}
	
	TRACE(__FUNCTION__ " closing accepting socket");
	// why is this try necessarily ?
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_socket);
	}
	catch (...) {}
}

void TrivyaServer::server()
{
	bindAndListen();

	// create new thread for handling message
	std::thread tr(&TrivyaServer::handleRecievedMessages, this);
	tr.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		TRACE("accepting client...");
		accept();
	}

}
void TrivyaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	TRACE("binded");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	TRACE("listening...");
}
void TrivyaServer::accept()
{
	SOCKET client_socket = ::accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	TRACE("Client accepted !");
	// create new thread for client	and detach from it
	std::thread tr(&TrivyaServer::clientHandler, this, client_socket);
	tr.detach();
}
void TrivyaServer::clientHandler(SOCKET sc)
{
	//add new messages to queue by msg code
	RecievedMessage* rm = nullptr;
	try
	{
		int msgCode = Helper::getMessageTypeCode(sc);
		while (msgCode != 0 && msgCode != CS_EXIT)
		{
			cout << "Msg CODE: " << msgCode << endl;
			rm = buildRecieveMessage(sc, msgCode);
			addRecievedMessage(rm);
			msgCode = Helper::getMessageTypeCode(sc);
		}
		rm = buildRecieveMessage(sc, CS_EXIT);
		addRecievedMessage(rm);
	}
	catch (...)
	{
		rm = buildRecieveMessage(sc, 299);
		addRecievedMessage(rm);
	}
}
void TrivyaServer::safeDeleteUser(RecievedMessage* rm)
{
	//handling signout and closing socket
	handleSignout(rm);
	TRACE(__FUNCTION__ " closing accepting socket");
	try
	{
		::closesocket(rm->getSock());
	}
	catch (...) {}
}

User* TrivyaServer::handleSignin(RecievedMessage* rm)
{
	if (!_db.isUserAndPassMatch(rm->getValues().at(0), rm->getValues().at(1))) //check if user and pass match
	{
		Helper::sendData(rm->getSock(), "1021");
		return nullptr;
	}
	else if (getUserByName(rm->getValues().at(0))) //check if user is already connected
	{
		Helper::sendData(rm->getSock(), "1022");
		return nullptr;
	}
	else
	{
		//creats new user and insert it to connected usrs
		User* u=new User(rm->getValues().at(0), rm->getSock());
		rm->setUser(u);
		_connectedUsers.insert(pair<SOCKET, User*>(rm->getSock(), rm->getUser()));
		Helper::sendData(rm->getSock(), "1020");
		return u;
	}
}
bool TrivyaServer::handleSignup(RecievedMessage* rm)
{
	if (!Validator::isPasswordValid(rm->getValues().at(1))) //check if pass vaild
	{
		Helper::sendData(rm->getSock(), "1041");
	}
	else if (!Validator::isUsernameValid(rm->getValues().at(0))) //check if username vaild
	{
		Helper::sendData(rm->getSock(), "1043");
	}
	else if (_db.isUserExist(rm->getValues().at(0))) //check if user exist
	{
		Helper::sendData(rm->getSock(), "1042");
	}
	else
	{
		if (!_db.addNewUser(rm->getValues().at(0), rm->getValues().at(1), rm->getValues().at(2))) //adding new user to db and check if succsedd
		{
			Helper::sendData(rm->getSock(), "1044");
		}
		else
		{
			Helper::sendData(rm->getSock(), "1040"); //all good
			return true;
		}
	}
	return false;
}
void TrivyaServer::handleSignout(RecievedMessage* rm)
{
	if (rm->getUser()!=nullptr)
	{
		_connectedUsers.erase(rm->getSock()); //removing from list
		handleCloseRoom(rm);
		handleLeaveGame(rm);
		handleLeaveGame(rm);
	}
}

void TrivyaServer::handleLeaveGame(RecievedMessage* rm)
{
	if (rm->getUser()->leaveGame())
	{
		delete rm->getUser()->getGame(); //delete the game
	}
}
void TrivyaServer::handleStartGame(RecievedMessage* rm)
{
	try
	{
		rm->getUser()->setGame(new Game(rm->getUser()->getRoom()->getUsers(), rm->getUser()->getRoom()->getQuestionNo(),_db)); //creates new game
	}
	catch (...)
	{
		rm->getUser()->send("1180");
		return;
	}
	rm->getUser()->closeRoom();
	rm->getUser()->getGame()->sendFirstQuestion(); //starting the game
	
}
void TrivyaServer::handlePlayerAnswer(RecievedMessage* rm)
{
	if (rm->getUser()->getGame() != nullptr)//check if there is a game
	{
		if (!(rm->getUser()->getGame()->handleAnswersFromUser(rm->getUser(), stoi(rm->getValues()[0]), stoi(rm->getValues()[1]))))//call to handleAnswersFromUser with the arguments from the massege
		{
			delete rm->getUser()->getGame();//release from memory
		}
	}
}

bool TrivyaServer::handleCreateRoom(RecievedMessage* rm)
{
	if (rm->getUser() != nullptr)
	{
		_roomIdSequence += 1;
		rm->getUser()->createRoom(_roomIdSequence, rm->getValues().at(0), stoi(rm->getValues().at(1)), stoi(rm->getValues().at(2)), stoi(rm->getValues().at(3))); //create new room by params
		_roomsList.insert(pair<int, Room*>(rm->getUser()->getRoom()->getId() , rm->getUser()->getRoom())); //insert it to list
		return true;
	}
	return false;
}
bool TrivyaServer::handleCloseRoom(RecievedMessage* rm)
{
	if (rm->getUser()->getRoom() == nullptr) //checks if there is a game
		return false;
	else
	{
		int id = rm->getUser()->getRoom()->getId();
		if (rm->getUser()->closeRoom() != -1) //check if close room sucssedd
		{
			_roomsList.erase(id);
			return true;
		}
		else
			return false;
	}
}
bool TrivyaServer::handleJoinRoom(RecievedMessage* rm)
{
	Room* room = getRoomById(stoi(rm->getValues().at(0)));
	if (rm->getUser() == nullptr)
		return false;
	else
	{
		if (room == nullptr)
		{
			Helper::sendData(rm->getSock(),"1102");
			return false;
		}
		else
		{
			rm->getUser()->joinRoom(getRoomById(stoi(rm->getValues().at(0))));
			return true;
		}
		
	}
}
bool TrivyaServer::handleLeaveRoom(RecievedMessage* rm)
{
	if (rm->getUser() == nullptr)
		return false;
	else
	{
		if (rm->getUser()->getRoom() == nullptr)
		{
			return false;
		}
		else
		{
			rm->getUser()->leaveRoom();
		}

	}
}
void TrivyaServer::handleGetUsersInRoom(RecievedMessage* rm)
{
	Room* tmp = getRoomById(stoi(rm->getValues().at(0)));
	if (tmp == nullptr)
	{
		rm->getUser()->send("1080");
	}
	else
	{
		rm->getUser()->send(tmp->getUsersListMessage());
	}
}
void TrivyaServer::handleGetRooms(RecievedMessage* rm)
{
	string s = "106";
	s +=Helper::getPaddedNumber(_roomsList.size(), 4);
	for each (pair<int, Room*> p in _roomsList)
	{
		s += Helper::getPaddedNumber(p.first, 4);
		s += Helper::getPaddedNumber(p.second->getName().length(), 2);
		s += p.second->getName();
	}
	Helper::sendData(rm->getSock(), s);
}

void TrivyaServer::handleGetBestScores(RecievedMessage* rm)
{
	string msg = "124";
	DataBase d;
	vector<string> v = d.getBestScores();
	for (int i = 0; i < 9; i++)
	{
		if (i == 0 || i == 3 || i == 6) //length
			msg += Helper::getPaddedNumber(stoi(v[i]), 2);
		else if (i == 1 || i == 4 || i == 7) //username
			msg += v[i];
		else if (i == 2 || i == 5 || i == 8)
			msg += Helper::getPaddedNumber(stoi(v[i]), 6);
	}
	Helper::sendData(rm->getSock(), msg);
}

void TrivyaServer::handleGetPersonalStatus(RecievedMessage* rm)
{
	string msg = "126";
	vector<string> v = _db.getPersonalStatus(rm->getUser()->getUsername());
	for (int i = 0; i < 4; i++)
	{
		if (i == 0) // the number of games
			msg += Helper::getPaddedNumber(stoi(v[i]), 4);
		else if (i == 1 || i == 2) // the correct or wrong answers
			msg += Helper::getPaddedNumber(stoi(v[i]), 6);
		else if (i == 3) // the time
		{
			msg += Helper::getPaddedNumber((stoi(v[i])*100), 4);
		}
	}
	Helper::sendData(rm->getSock(), msg);
}

void TrivyaServer::addRecievedMessage(RecievedMessage* rm)
{
	unique_lock<mutex> lck(_mtxRecievedMessages);
	_messageHandler.push(rm);
	lck.unlock();
	_msgQueueCondition.notify_all();
}

RecievedMessage* TrivyaServer::buildRecieveMessage(SOCKET sc, int num)
{
	RecievedMessage* msg = nullptr;
	vector<string> values;
	string userName;
	if (num == CS_SIGNIN)
	{
		int userSize = Helper::getIntPartFromSocket(sc, 2);
		userName = Helper::getStringPartFromSocket(sc, userSize);
		int passSize = Helper::getIntPartFromSocket(sc, 2);
		string password = Helper::getStringPartFromSocket(sc, passSize);
		values.push_back(userName);
		values.push_back(password);
	}
	else if (num == CS_SIGNUP)
	{
		int userSize = Helper::getIntPartFromSocket(sc, 2);
		userName = Helper::getStringPartFromSocket(sc, userSize);
		int passSize = Helper::getIntPartFromSocket(sc, 2);
		string password = Helper::getStringPartFromSocket(sc, passSize);
		int mailSize = Helper::getIntPartFromSocket(sc, 2);
		string mail = Helper::getStringPartFromSocket(sc, mailSize);
		values.push_back(userName);
		values.push_back(password);
		values.push_back(mail);
	}
	else if (num == CS_ROOMUSERLIST || num==CS_JOINROOM)
	{
		string roomId = Helper::getStringPartFromSocket(sc, 4);
		values.push_back(roomId);
	}
	else if (num == CS_MAKEROOM)
	{
		int roomSize = Helper::getIntPartFromSocket(sc, 2);
		string roomName = Helper::getStringPartFromSocket(sc, roomSize);
		string playersNum = Helper::getStringPartFromSocket(sc, 1);
		string num = Helper::getStringPartFromSocket(sc, 2);
		string time = Helper::getStringPartFromSocket(sc, 2);
		values.push_back(roomName);
		values.push_back(playersNum);
		values.push_back(num);
		values.push_back(time);
	}
	else if (num == CS_SENDANSWER)
	{
		string questionNo = Helper::getStringPartFromSocket(sc, 1);
		string time = Helper::getStringPartFromSocket(sc, 2);
		values.push_back(questionNo);
		values.push_back(time);
	}
	msg = new RecievedMessage(sc, num, values);
	return msg;
}
User* TrivyaServer::getUserByName(string name)
{
	for each (auto x in _connectedUsers)
	{
		if (x.second->getUsername() == name)
			return x.second;
	}
	return nullptr;
}

User* TrivyaServer::getUserBySocket(SOCKET sc)
{
	for each (auto x in _connectedUsers)
	{
		if (x.first == sc)
			return x.second;
	}
	return nullptr;
}
Room* TrivyaServer::getRoomById(int id)
{
	for each (auto x in _roomsList)
	{
		if (x.first == id)
			return x.second;
	}
	return nullptr;
}


void TrivyaServer::handleRecievedMessages()
{
	int msgCode = 0;
	SOCKET clientSock = 0;
	string userName;
	while (true)
	{
		try
		{
			unique_lock<mutex> lck(_mtxRecievedMessages);

			// Wait for clients to enter the queue.
			if (_messageHandler.empty())
				_msgQueueCondition.wait(lck);

			// in case the queue is empty.
			if (_messageHandler.empty())
				continue;

			RecievedMessage* currMessage = _messageHandler.front();
			_messageHandler.pop();
			lck.unlock();

			// Extract the data from the tuple.
			clientSock = currMessage->getSock();
			msgCode = currMessage->getMessageCode();
			currMessage->setUser(getUserBySocket(clientSock));
			if (msgCode == CS_SIGNIN)
				handleSignin(currMessage);
			else if (msgCode == CS_CLOSEROOM)
				handleCloseRoom(currMessage);
			else if (msgCode == CS_JOINROOM)
				handleJoinRoom(currMessage);
			else if (msgCode == CS_LEAVEROOM)
				handleLeaveRoom(currMessage);
			else if (msgCode == CS_ROOMLIST)
				handleGetRooms(currMessage);
			else if (msgCode == CS_SIGNUP)
				handleSignup(currMessage);
			else if (msgCode == CS_SIGNOUT)
				handleSignout(currMessage);
			else if (msgCode == CS_ROOMUSERLIST)
				handleGetUsersInRoom(currMessage);
			else if (msgCode == CS_MAKEROOM)
				handleCreateRoom(currMessage);
			else if (msgCode == CS_STARTGAME)
				handleStartGame(currMessage);
			else if (msgCode == CS_SENDANSWER)
				handlePlayerAnswer(currMessage);
			else if (msgCode == CS_BESTSCORES)
				handleGetBestScores(currMessage);
			else if (msgCode == CS_PERSONALSTATUS)
				handleGetPersonalStatus(currMessage);
			else
				safeDeleteUser(currMessage);
			delete currMessage;
		}
		catch (...)
		{

		}
	}
}